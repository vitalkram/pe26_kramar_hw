function creatNewUser() {
    let firstName = prompt('Enter name');
    let lastName = prompt('Enter last name');
    let birthDay = prompt('Enter your birth date', 'dd.mm.yyyy');
    let dateArr = birthDay.split('.');
    let newUsDay = {
        day: dateArr[1],
        month: dateArr[0],
        year: dateArr[2],
    };

    const newUser = {
        name: firstName,
        lastname: lastName,
        birthday: birthDay,

        getAge: function () {
            let today = new Date();
            let usDate = new Date(`${dateArr[1]}.${dateArr[0]}.${dateArr[2]}`);
            let age = today.getFullYear() - usDate.getFullYear();
            if (today.getMonth() < usDate.getMonth()) {
                return --age;
            }
            if (today.getMonth() === usDate.getMonth() && today.getDate() > usDate.getDate()) {
                return --age;
            }
            return age;
        },
        getPassword: function () {
            return this.name[0].toUpperCase() + this.lastname.toLowerCase() + newUsDay.year;

        }

    };
    return newUser;
}

const newUser = creatNewUser();

console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());
