window.addEventListener('keyup', (ev) => {
   let allBtn = document.querySelectorAll('.btn');
   let eventKey = ev.key.toLowerCase();
   allBtn.forEach((btn) => {
      if (eventKey === btn.textContent.toLowerCase()) {
          if (!btn.classList.contains('press')) {
              btn.classList.add('press');
          }
      } else {
          if (btn.classList.contains('press')) {
              btn.classList.remove('press');
          }
      }
   });
});