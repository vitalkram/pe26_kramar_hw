let userNumber = 0;
do {
    userNumber = +prompt("Enter number");
} while (!Number.isInteger(userNumber));

if (userNumber < 5) {
    console.log("Sorry, no numbers");
} else {
    for (let i = 5; i <= userNumber; i++) {
        if (i % 5 == 0)
            console.log(i);
    }
}