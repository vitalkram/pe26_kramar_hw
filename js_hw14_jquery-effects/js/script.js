const $navList = $('.nav-list');
const $linkToTop = $('.link-to-top');
const $slideTog = $('.slide-tog');
const $hotNews = $('.hot-news');

function scroll(element, offsetTop) {
    if (offsetTop) {
        $('html, body').animate({scrollTop: offsetTop}, 'slow');
    } else {
        let $element = $(element);
        elementScrollName = $element.attr('href').replace('#', '');
        $('html, body').animate({scrollTop: $(`a[name=${elementScrollName}]`).offset().top}, 'slow');
    }
}

$navList.click(ev => {
   scroll(ev.target);
});

$linkToTop.click(ev => {
    scroll(ev.target, -10);
});

$(window).scroll(() => {
   if ($(window).scrollTop() > $(window).height()) {
       $linkToTop.css('display', 'block');
   } else {
       $linkToTop.css('display', 'none');
   }
});

$slideTog.click(() => {
    $hotNews.slideToggle();
});